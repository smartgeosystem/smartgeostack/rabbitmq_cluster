# Tools for deploy RabbitMQ cluster


## docker-compose
```bash
# Get compose file
git clone git@gitlab.com:smartgeosystem/smartgeostack/rabbitmq_cluster.git
cd ./rabbitmq_cluster/docker-compose
# Create configs
cp ./common.env.template ./common.env
cp ./master.env.template ./master.env
cp ./node.env.template ./node.env
# Edit env files...
docker-compose up -d
```
